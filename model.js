import Mongoose from 'mongoose'

const OrganizationSchema = {
  name: {type: String, unique: true},
  email: {type: String}
}
const Organization = Mongoose.model('Organization', OrganizationSchema)

export {
  Organization
}
