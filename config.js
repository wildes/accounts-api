export const httpPort = process.env.PORT || 4000
export const mongoUri = process.env.MONGODB_URI || 'mongodb://localhost/test'
