import { schemaComposer } from 'graphql-compose'
import { composeWithMongoose } from 'graphql-compose-mongoose'
import { Organization } from './model'

const customizationOptions = {}
const OrganizationTC = composeWithMongoose(Organization, customizationOptions)

schemaComposer.Query.addFields({
  organizations: OrganizationTC.getResolver('findMany'),
  organizationById: OrganizationTC.getResolver('findById'),
  organizationCount: OrganizationTC.getResolver('count'),
  organizationConnection: OrganizationTC.getResolver('connection'),
  organizationPagination: OrganizationTC.getResolver('pagination')
})

schemaComposer.Mutation.addFields({
  createOrganization: OrganizationTC.getResolver('createOne'),
  updateOrganizationById: OrganizationTC.getResolver('updateById'),
  deleteOrganizationById: OrganizationTC.getResolver('removeById'),
})

const graphqlSchema = schemaComposer.buildSchema()
export default graphqlSchema
