import express from 'express'
import session from 'express-session'
import Keycloak from 'keycloak-connect'
import {
  ApolloServer, AuthenticationError
} from 'apollo-server-express'
import {httpPort} from './config'
import './mongooseConnection'
import schema from './schema'

const memoryStore = new session.MemoryStore();

const kcConfig = {
  realm: "demo",
  clientId: "my-react-app",
  serverUrl: "https://auth.gynlabs.io/auth",
};

const keycloak = new Keycloak({ store: memoryStore }, kcConfig);

const app = express()
app.set('trust proxy',true)
app.use( session({
  secret: 'aaslkdhlkhsd',
  resave: false,
  saveUninitialized: true,
  store: memoryStore,
}))

//app.use(keycloak.middleware())
//app.use(keycloak.protect());

const server = new ApolloServer({ 
    schema,
    playground: process.env.NODE_ENV !== 'production',
    context: async ({req}) => {
    }
})

server.applyMiddleware({ app })

app.listen({ port: httpPort }, () =>
  console.log(`🚀 Server ready at http://localhost:${httpPort}${server.graphqlPath}`)
)
